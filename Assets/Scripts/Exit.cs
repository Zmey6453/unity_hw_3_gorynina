﻿using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Cube))]
public class Exit : MonoBehaviour
{
    internal Cube cube;
    void Awake()
    {
        cube = GetComponent<Cube>();
    }

    Player player;

    Color exit_color = Color.white;


    [SerializeField]
    public float speed_rotation = 50;
    public bool PlayerIsReadyForExit()
    {
        return player.cube.color.Equals(exit_color);
    }

    void Update()
    {
        float radius_of_visibility = 8;
        float distance_to_player = (player.transform.position - transform.position).magnitude;
        float delta_alpha = Mathf.Max(0, distance_to_player - radius_of_visibility / 2);
        delta_alpha = Mathf.Min(radius_of_visibility, delta_alpha) / radius_of_visibility;

        Color temp_color = cube.color;
        temp_color.a = PlayerIsReadyForExit() ? 1 : delta_alpha;
        cube.color = temp_color;

        if (PlayerIsReadyForExit())
            transform.rotation = Quaternion.identity;
        else
            transform.Rotate(0.0f, Time.deltaTime * speed_rotation, 0.0f);
    }

    public void Init(Color color, Player player)
    {
        exit_color = color;
        cube.color = color;
        this.player = player;
    }
    void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<Player>();
        if (player != null && PlayerIsReadyForExit())
        {
            GetComponent<Animator>().enabled = true;
            GameObject door = GameObject.Find("ActiveDoor");
            door.transform.GetChild(0).gameObject.SetActive(true);
            Game.GameOver(true);
            SceneManager.LoadScene(1, LoadSceneMode.Additive);

        }

    }
}
