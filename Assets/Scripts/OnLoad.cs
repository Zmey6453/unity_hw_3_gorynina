﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnLoad : MonoBehaviour
{
    public Vector3 end = new Vector3(0, 0, 0);
    public Vector3 start;
    public float speed = 50.0f;
   
    void Start()
    {
        start = transform.position;
    }


    void Update()
    {
        transform.position = Vector3.Lerp(start, end, speed*Time.deltaTime*10.0f);
        start = transform.position;
    }
}
