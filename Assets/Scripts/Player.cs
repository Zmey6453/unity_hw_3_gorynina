﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Player : MonoBehaviour
{
    internal Cube cube;
    void Awake()
    {
        cube = GetComponent<Cube>();
    }
    
    void Start()
    {
        BoxCollider collider = gameObject.GetComponent<BoxCollider>();
        collider.isTrigger = true;   
        
        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = true;
        rb.angularDrag = 0;
    }

    void Update()
    {
        cube.Move(new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")));
    }

    public void SwallowColor(Color color)
    {
        Color new_color = cube.color - Color.white*0.2f + color * 0.6f;
        new_color.r = Mathf.Clamp01(new_color.r);
        new_color.g = Mathf.Clamp01(new_color.g);
        new_color.b = Mathf.Clamp01(new_color.b);
        new_color.a = 1;
        cube.color = new_color;
    }
}
