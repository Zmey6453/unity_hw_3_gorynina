﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
  private static GameState state;
  enum GameState
  {
    GAME,
    VICTORY,
    DEFEAT,
    FREE
  }
  public GameObject player_go;
  public GameObject exit_go;
  public GameObject exit_pulse_go;
  public GameObject marker_go;
  public GameObject signal_for_exit_ui;
  public Text lefttime_ui;
  public RawImage arrow_ui;
  public GameObject camera_go;
  
  [SerializeField]
  public List<GameObject> border = new List<GameObject>();  

  static float start_game_stamp;
  float game_time = 30;
  
  float spawn_markers_stamp;
  float spawn_markers_cooldown = 0.5f;
  
  Exit exit;
  Player player;
  Marker first_marker;
  Color exit_color;
  PlayerFollowing player_following;
  bool is_destroyed = false;

  Animator lefttime_animator;
  
  void Start()
  {
    state = GameState.GAME;
    marker_go.SetActive(false);
    
    exit_color = GetRandomColor(1);
    
   
    first_marker = CreateMarker();
    first_marker.Init(exit_color, first_marker.transform.position, Vector3.zero, this);
    
    player = player_go.AddComponent<Player>();
    //player.cube.color = exit_color;
    
    exit = exit_go.AddComponent<Exit>();
    exit.Init(exit_color, player);

    player_following = camera_go.AddComponent<PlayerFollowing>();
    player_following.Init(player.gameObject);
    
    lefttime_animator = lefttime_ui.gameObject.GetComponent<Animator>();
    lefttime_animator.SetBool("Alert", false);
    lefttime_animator.SetTrigger("Cancel");
    
    Color pulse_color = exit_color;
    pulse_color.a = 0.25f;
    ColorizeGo(exit_pulse_go, pulse_color);

    for (int i = 0; i < border.Count; i++)
      ColorizePanel(border[i], exit_color);
      
    start_game_stamp = Time.time;
  }

  void ColorizeGo(GameObject go, Color color)
  {
    Renderer renderer = go.GetComponent<Renderer>();
    var tempMaterial = new Material(renderer.sharedMaterial);
    tempMaterial.color = color;
    renderer.sharedMaterial = tempMaterial;
  }
  
  void ColorizePanel(GameObject go, Color color)
  {
    Image img = go.GetComponent<Image>();
    img.color = color;
  }
  
  float GetLeftTime()
  {
    return Mathf.Max(0, start_game_stamp + game_time - Time.time);
  }

  Marker CreateMarker()
  {
    GameObject marker = Instantiate(marker_go);
    marker.SetActive(true);
    return marker.AddComponent<Marker>(); 
  }

  float exit_arrow_radius = 20;
  float spawn_radius = 40;
  Rect spawn_rect = Rect.zero;
    void Update()
    {
   
            if (!IsGame() && !IsFree())
        {
            lefttime_ui.text = "";
            arrow_ui.enabled = false;
            exit_pulse_go.SetActive(false);
            signal_for_exit_ui.SetActive(false);
            return;
        }


        Vector3 player_pos = player.transform.position;

        if (IsGame())
        {
            if (GetLeftTime() == 0)
                GameOver(false);
            else
                lefttime_animator.SetBool("Alert", GetLeftTime() < 5);

            lefttime_ui.text = "Left Time: " + string.Format("{0:0.00}", GetLeftTime());
            Vector3 exit_pos = exit.transform.position;
            Vector3 center_screen_pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2)) - player_following.offset;

            signal_for_exit_ui.SetActive(exit.PlayerIsReadyForExit());
            exit_pulse_go.SetActive(exit.PlayerIsReadyForExit());
            arrow_ui.enabled = (exit_pos - center_screen_pos).magnitude > exit_arrow_radius;

            Vector3 player_screen_pos = Camera.main.WorldToScreenPoint(player_pos);
            Vector3 exit_screen_pos = Camera.main.WorldToScreenPoint(exit_pos);
            player_screen_pos.z = 0;
            exit_screen_pos.z = 0;
            Vector3 dir_to_exit = exit_screen_pos - player_screen_pos;
            Vector3 exit_arrow_screen_pos = exit_screen_pos - dir_to_exit.normalized * 20;

            arrow_ui.transform.position = new Vector3(Mathf.Clamp(exit_arrow_screen_pos.x, 20, Screen.width - 20), Mathf.Clamp(exit_arrow_screen_pos.y, 20, Screen.height - 20), 0);
            arrow_ui.transform.right = (exit_screen_pos - arrow_ui.transform.position).normalized;
        }
        if (IsFree())
        {
            spawn_radius = 50;
            spawn_markers_cooldown = 0.8f;
            arrow_ui.enabled = false;
            exit_pulse_go.SetActive(false);
            signal_for_exit_ui.SetActive(false);
            lefttime_ui.enabled = false;

            //для уничтожения маркеров при переходе в свободную зону, но мне больше нравится без этой части 
            //(хотя некоторые маркеры все равно пропадут из-за столкновений с объектами уровня)
            //if (!is_destroyed) for (int counter = 0; counter < GameObject.FindGameObjectsWithTag("Marker").Length; counter++)
            //    {
            //        Destroy(GameObject.FindGameObjectsWithTag("Marker")[counter]);
            //    }
            //is_destroyed = true;
        }


        if (first_marker != null || spawn_markers_stamp + spawn_markers_cooldown > Time.time)
      return;
    
    spawn_markers_stamp = Time.time;
    spawn_rect.position = new Vector2(player_pos.x - spawn_radius, player_pos.z - spawn_radius);
    spawn_rect.size = new Vector2(spawn_radius*2, spawn_radius*2);
    
    Vector3 start_pos;
    Vector3 direction; 
    float random_pos = 2 * spawn_radius * Random.value; 
      
    switch (Random.Range(0, 4))
    {
        case 0:
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back*random_pos;
          direction = Vector3.right;
          break;
        case 1:
          start_pos = new Vector3(spawn_rect.xMax, 0, spawn_rect.yMax) + Vector3.back*random_pos;
          direction = Vector3.left;
          break;
        case 2:
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.right*random_pos;
          direction = Vector3.back;
          break;
        default :
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMin) + Vector3.right*random_pos;
          direction = Vector3.forward;
          break;
    }
        Color color = state == GameState.GAME ? GetRandomColor() : GetRandomColor(1);
        Marker random_marker = CreateMarker();
    random_marker.Init(color, start_pos, direction, this);
  }

  private List<Color> colors = new List<Color> {Color.white, Color.red, Color.green, Color.blue};
  private Color GetRandomColor(int start_idx = 0)
  {
    return colors[Random.Range(start_idx,colors.Count)];
  }
  
  void OnDrawGizmos() {
    Gizmos.color = Color.green;
    Gizmos.DrawCube(new Vector3(spawn_rect.center.x, 0, spawn_rect.center.y), new Vector3(spawn_rect.width, 0, spawn_rect.height) );
  }

  void OnGUI() {
    if (!IsGame()&& state==GameState.DEFEAT)
    {
      GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
      GUI.Box (new Rect (0,0,100,100), "DEFEAT");
      if(GUI.Button(new Rect(10, 40, 80, 30), "Play again!"))
      {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
      }
      GUI.EndGroup ();
    }
  }
  
  public static bool IsGame()
  {
    return state == GameState.GAME;
  }

  public static bool IsFree()
  {
    return state == GameState.FREE;
  }

    public static void GameOver(bool is_victory)
  {
    state = is_victory ? GameState.FREE : GameState.DEFEAT;
  }

  internal void IncreaseTime(int seconds)
  {
    start_game_stamp += seconds;
    
    lefttime_animator.SetTrigger("Shake");
  }  
}
