﻿using System.Collections;
using UnityEngine;

public class Cube : MonoBehaviour
{
    private Renderer renderer_;
    private Renderer renderer
    {
        get
        {
            if(renderer_ == null)
                renderer_ = gameObject.GetComponent<Renderer>();

            return renderer_;
        }
    }

    public Color color
    {
        set
        {
            var tempMaterial = new Material(renderer.sharedMaterial);
            tempMaterial.color = value;
            renderer.sharedMaterial = tempMaterial;
        }
        get { return renderer.sharedMaterial.color; }
    }
    
    [SerializeField]
    public int frames = 10;
    
    private float step_size;
    private Vector3 axis_direction = Vector3.zero;
    private Vector3 current_position = Vector3.zero;
    private bool toggle = false;
    
    private IEnumerator coroutine_animation;
    
    // Start is called before the first frame update
    void Start()
    {
        step_size = transform.localScale.y;

    }
    
    private Vector3 GetNextDirection(Vector3 axis)
    {
        if (axis.sqrMagnitude <= 1) 
            return Vector3.one;
        
        toggle = !toggle;
        return toggle ? Vector3.forward : Vector3.right;
    }
    
    IEnumerator DoRotation()
    {
        var edge_delta = step_size * 0.5f;
        Vector3 around_pos = current_position + axis_direction * edge_delta;
        around_pos.y = 0;
        
        Vector3 dir_rotating = Vector3.Cross(Vector3.up, axis_direction);
        
        float angle = 90.0f;
        float delta = angle/frames;

        while (angle > 0)
        {
            angle -= delta;
            transform.RotateAround(around_pos, dir_rotating, delta);
            yield return null;
        }

        axis_direction = Vector3.zero;
        coroutine_animation = null; 
    }
    
    private void StartRotation()
    {
        coroutine_animation = DoRotation(); 
        StartCoroutine(coroutine_animation);
    }
    
    public void Rotate(Vector3 axis, float width)
    {
        if(coroutine_animation != null)
            return;
        
        step_size = width;
        current_position = transform.position;

        axis.x = axis.x == 0 ? 0 : Mathf.Abs(axis.x)/axis.x;
        axis.y = axis.y == 0 ? 0 : Mathf.Abs(axis.y)/axis.y;
        axis.z = axis.z == 0 ? 0 : Mathf.Abs(axis.z)/axis.z;
        axis_direction = Vector3.Scale(axis, GetNextDirection(axis));
            
        StartRotation();
    }
    
    public void Move(Vector3 axis)
    {
        if (!Game.IsGame()&& !Game.IsFree())
            return;
        
        if(axis != Vector3.zero)
            Rotate(axis, step_size);
    }
}
