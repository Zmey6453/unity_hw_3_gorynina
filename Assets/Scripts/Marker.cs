﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Marker : MonoBehaviour
{    
    internal Cube cube;
    void Awake()
    {
        cube = GetComponent<Cube>();
    }

    private Vector3 speed;
    private Game game;
    
    public void Init(Color color, Vector3 position, Vector3 speed, Game game)
    {
        this.game = game;
        cube.color = color;
        transform.position = position;
        this.speed = speed;
    }
    
    void Update()
    {
        if (!Game.IsFree())
        cube.Move(speed.normalized);
    }
    
    void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {
            if (cube.color != Color.white)
            {
                player.SwallowColor(cube.color);
            }
            else
            {
                game.IncreaseTime(10);
            }
         }   

            Destroy(gameObject);
        
    }

    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
